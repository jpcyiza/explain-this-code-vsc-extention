import * as vscode from 'vscode';

export default function getSelectedEditorText() {
  const editor = vscode.window.activeTextEditor;
  return editor?.document.getText(editor?.selection);
}
