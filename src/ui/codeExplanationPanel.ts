import * as vscode from 'vscode';
import { CODE_EXPLAIN_GUI_CMD_KEY } from '../constants';

export default class CodeExplanationPanel {
  readonly selectedCode: string;

  readonly explanation: string;

  private readonly _panel: vscode.WebviewPanel;

  private static currentPanel: CodeExplanationPanel | undefined;

  public static createOrShow(selectedCode: string, explanation: string) {
    const column = vscode.window.activeTextEditor
      ? vscode.window.activeTextEditor.viewColumn
      : undefined;

    if (CodeExplanationPanel.currentPanel) {
      CodeExplanationPanel.currentPanel._panel.reveal(column);
      return;
    }

    const panel = vscode.window.createWebviewPanel(
      CODE_EXPLAIN_GUI_CMD_KEY,
      'Code Explanation',
      column || vscode.ViewColumn.Two,
      {
        enableScripts: true,
        localResourceRoots: [],
      },
    );

    CodeExplanationPanel.currentPanel = new CodeExplanationPanel(panel, selectedCode, explanation);
  }

  private constructor(panel: vscode.WebviewPanel, selectedCode: string, explanation: string) {
    this.selectedCode = selectedCode;
    this.explanation = explanation;

    this._panel = panel;
    this._panel.onDidDispose(() => {
      CodeExplanationPanel.currentPanel = undefined;
    });

    this._panel.webview.html = this._getHtmlForWebview();
  }

  private _getHtmlForWebview(): string {
    return `
      <!DOCTYPE html>
      <html  lang="en">
        <head>
          <meta charset="UTF-8">
          <meta name="viewport" content="width=device-width, initial-scale=1.0">
          <title>Explain this code - Code explanation</title>
        </head>
        <body>
          <section>
            <h1>Code Explanation</h1>
            <div>
                <code>${this.selectedCode}</code>
            </div>
            <div>
                <h4>Here is what we have</h4>
                <p>${this.explanation}</p>
            </div>
          </section>
        </body>
      </html>
    `;
  }
}
