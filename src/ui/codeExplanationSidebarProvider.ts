import * as vscode from "vscode";
import AiApi from "../services/aiApi";
import getSelectedEditorText from "../utils/getSelectedEditorText";

export default class CodeExplanationSidebarProvider implements vscode.WebviewViewProvider {
  private _view?: vscode.WebviewView;

  public explanation?: string | null;

  public selectedCode?: string | null;

  constructor(private readonly _extensionUri: vscode.Uri) {}

  async resolveWebviewView(
    webviewView: vscode.WebviewView,
  ) {
    this._view = webviewView;

    this._view.webview.options = {
      // Allow scripts in the webview
      enableScripts: true,

      localResourceRoots: [
        this._extensionUri
      ],
    };

    await this.builtExplainCodePage();

    this._view.webview.onDidReceiveMessage(async (data) => {
      switch (data.type) {
        case "message": {
          if (!data.value) {
            return;
          }
          vscode.window.showInformationMessage(data.value);
          break;
        }
        case "openApp": {
          await vscode.commands.executeCommand(
            'vscodevuerollup:openVueApp', { ...data }
          );
          break;
        }
        case "onInfo": {
          if (!data.value) {
            return;
          }
          vscode.window.showInformationMessage(data.value);
          break;
        }
        case "onError": {
          if (!data.value) {
            return;
          }
          vscode.window.showErrorMessage(data.value);
          break;
        }
      }
    });
  }

  public revive(panel: vscode.WebviewView) {
    this._view = panel;
  }

  public focus() {
    this._view?.show();
  }

  public async builtExplainCodePage() {
    if(!this._view) {return;};

    this.selectedCode = getSelectedEditorText();

    if(!this.selectedCode) {
      this._view.webview.html = this._noCodeSelectedPage();
    }

    this.explanation = await new AiApi().getCodeExplanation(this.selectedCode);
  
    if(!this.explanation) {
      this._view.webview.html = this._noExplainationFoundPage();
      await vscode.window.showInformationMessage('No code selected!');
    } else {
      this._view.webview.html = this._explainCodePage();
    }
  }

  public sendMessage() {
    return vscode.window.showInputBox({
      prompt: 'Enter your message',
      placeHolder: 'Hey Sidebar!'
    }).then(value => {
      if (value) {
        this.postWebviewMessage({
          command: 'message',
          data: value,});
      }
    });
  }

  private postWebviewMessage(msg: {
    command: string,
    data?: any
  }) {
    vscode.commands.executeCommand(
      'workbench.view.extension.vscodevuerollup-sidebar-view');
    vscode.commands.executeCommand('workbench.action.focusSideBar');

    this._view?.webview.postMessage(msg);
  }

  private _explainCodePage(): string {
    return `
      <!DOCTYPE html>
      <html  lang="en">
        <head>
          <meta charset="UTF-8">
          <meta name="viewport" content="width=device-width, initial-scale=1.0">
          <title>Explain this code - Code explanation</title>
        </head>
        <body>
          <section>
            <h1>Code Explanation</h1>
            <div>
                <code>${this.selectedCode}</code>
            </div>
            <div>
                <h4>Here is what we have</h4>
                <p>${this.explanation}</p>
            </div>
          </section>
        </body>
      </html>
    `;
  }

  private _noCodeSelectedPage(): string {
    return `
      <!DOCTYPE html>
      <html  lang="en">
        <head>
          <meta charset="UTF-8">
          <meta name="viewport" content="width=device-width, initial-scale=1.0">
          <title>Explain this code - Code explanation</title>
        </head>
        <body>
          <section>
            <h1>Code Explanation</h1>
            <div>
              <h4>No code selected!</h4>
            </div>
          </section>
        </body>
      </html>
    `;
  }

  private _noExplainationFoundPage(): string {
    return `
      <!DOCTYPE html>
      <html  lang="en">
        <head>
          <meta charset="UTF-8">
          <meta name="viewport" content="width=device-width, initial-scale=1.0">
          <title>Explain this code - Code explanation</title>
        </head>
        <body>
          <section>
            <h1>Code Explanation</h1>
            <div>
                <code>${this.selectedCode}</code>
            </div>
            <div>
                <h4>Here is what we have</h4>
                <p>No explaination found!</p>
            </div>
          </section>
        </body>
      </html>
    `;
  }
}
