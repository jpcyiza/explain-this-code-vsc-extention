import * as vscode from 'vscode';
import {CODE_EXPLAIN_GUI_CMD_KEY, GET_EXPLANATION_CMD_KEY} from '../constants';
import AiApi from '../services/aiApi';
import CodeExplanationSidebarProvider from "../ui/codeExplanationSidebarProvider";

export default class CommandsIndexes {

  private _context: vscode.ExtensionContext;

  private _explainCodeGuiProvider: CodeExplanationSidebarProvider;

  constructor(context: vscode.ExtensionContext) {
    this._context = context;
    this._explainCodeGuiProvider = new CodeExplanationSidebarProvider(this._context.extensionUri);
  }
  
  initCodeExplainPanelCmd(): vscode.Disposable {
    return vscode.window.registerWebviewViewProvider(
      CODE_EXPLAIN_GUI_CMD_KEY,
      this._explainCodeGuiProvider
    );
  }
  
  initGetExplanationCmd(): vscode.Disposable {
    return vscode.commands.registerCommand(
      GET_EXPLANATION_CMD_KEY,
      async () => {
          await vscode.commands.executeCommand(`${CODE_EXPLAIN_GUI_CMD_KEY}.focus`);
          await this._explainCodeGuiProvider.builtExplainCodePage();
      },
    );
    
  }
  
  async registerCommands(): Promise<void> {
    const disposables: Array<vscode.Disposable> = [
      this.initCodeExplainPanelCmd(),
      this.initGetExplanationCmd()
    ];
  
    disposables.forEach(disposable => {
      this._context.subscriptions.push(disposable);
    });
  }
}
