import getSelectedEditorText from '../utils/getSelectedEditorText';

export default class AiApi {
  // eslint-disable-next-line class-methods-use-this
  getCodeExplanation(selectedCode?: string): Promise<string> | Promise<null> {
    // The code inside here is a placeholder for when we will integrate the real api
    if(!selectedCode) {return Promise.resolve(null);}

    const firstWord = selectedCode.split(' ').filter(Boolean)[0];
    const explanation = `The 🦊says: Selected text has ${selectedCode.length} characters and starts with the word "${firstWord}"`;

    return Promise.resolve(explanation);

  }
}
