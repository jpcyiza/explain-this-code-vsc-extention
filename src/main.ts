// The module 'vscode' contains the VS Code extensibility API
// Import the module and reference it with the alias vscode in your code below
import * as vscode from 'vscode';
import CommandCenter from './commands';

export async function activate(context: vscode.ExtensionContext) {
  const commandCenter = new CommandCenter(context);
  await commandCenter.registerCommands();
}
