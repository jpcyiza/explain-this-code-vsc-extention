# GitLab explain this code Visual Studio Code extension

Extension for GitLab explain code feature.

## Features

Open the comand palette (`⇧⌘P`) and type "say hello" you should see the command "🦊 say hello". You should see the **The Tanuki 🦊 says hello! 👋** notification showing up. Success!

## How to install from this repo

### Generate an executable

1. install **Visual Studio Code Extensions** npm package `npm install -g vsce`
1. Clone the repo and run the following command folder its folder execute `vsce package` this will generate an `.vsix` named `gitlab-explain-this-code-0.0.1.vsix`

### Install the executable
1. make sure the `code` path is linked to VS Code CLI. If it is not follow these instructions to do it quick. https://stackoverflow.com/questions/29955500/code-is-not-working-in-on-the-command-line-for-visual-studio-code-on-os-x-ma
1. then run `code --install-extension gitlab-explain-this-code-0.0.1.vsix`

**Enjoy!**
